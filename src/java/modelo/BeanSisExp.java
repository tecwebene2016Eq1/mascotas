package modelo;

import java.util.ArrayList;
import java.util.List;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;

/**
 *
 * @author Alex
 */
@Named(value = "beanSisExp")
@RequestScoped
public class BeanSisExp {

    private int cont;
    private String pregunta1;
    private String pregunta2;
    private String pregunta3;
    private String pregunta4;
    private String pregunta5;
    private String pregunta6;
    private String pregunta7;
    private String pregunta8;
    private String pregunta9;

    private String respuestas[] = {"A. No tengo tiempo definido, pero el espacio en casa es grande y no me preocupo",
        "B. por las noches, ya que salgo temprano a trabajar y llego después de las 6 pm",
        "C. Los fines de semana, por que otros miembros de la familia se encargaran entre semana",
        "D. Al menos dos horas al día, ya que mi trabajo en casa o cerca de casa me lo permite"

    };

    private List<String> lista = new ArrayList<String>();

    {

        getLista().add(getRespuestas()[0]);
        getLista().add(getRespuestas()[1]);
        getLista().add(getRespuestas()[2]);
        getLista().add(getRespuestas()[3]);

    }

    private String respuestas2[] = {"A. 50 a 100 pesos",
        "B. 100 a 200 pesos",
        "C. 200 a 400 pesos ",
        "D. Sin límite, ya que puedo asumir cualquier gasto imprevisto"

    };

    private List<String> lista2 = new ArrayList<String>();

    {

        getLista2().add(getRespuestas2()[0]);
        getLista2().add(getRespuestas2()[1]);
        getLista2().add(getRespuestas2()[2]);
        getLista2().add(getRespuestas2()[3]);

    }

    private String respuestas3[] = {"A. Por moda / para cruzarlo y vender a los cachorros ",
        "B. Porque mi hijo(a) insiste y desea tener un perrito",
        "C. Por seguridad mía y de mi casa ",
        "D. Porque amo a los animales y deseo darle un hogar a un perro que lo necesita"

    };

    private List<String> lista3 = new ArrayList<String>();

    {

        getLista3().add(getRespuestas3()[0]);
        getLista3().add(getRespuestas3()[1]);
        getLista3().add(getRespuestas3()[2]);
        getLista3().add(getRespuestas3()[3]);

    }

    private String respuestas4[] = {"A. ¡Me encanta! al menos 2 veces al año ",
        "B. He tenido 2 a 3 cambios de casa en toda mi vida",
        "C. Llevo más de 10 años en la misma casa",
        "D. Nunca me he cambiado de casa"

    };

    private List<String> lista4 = new ArrayList<String>();

    {

        getLista4().add(getRespuestas4()[0]);
        getLista4().add(getRespuestas4()[1]);
        getLista4().add(getRespuestas4()[2]);
        getLista4().add(getRespuestas4()[3]);

    }

    private String respuestas5[] = {"A. En un departamento pequeño ",
        "B. En el balcón",
        "C. En un departamento amplio ",
        "D. En una casa con jardín"

    };

    private List<String> lista5 = new ArrayList<String>();

    {

        getLista5().add(getRespuestas5()[0]);
        getLista5().add(getRespuestas5()[1]);
        getLista5().add(getRespuestas5()[2]);
        getLista5().add(getRespuestas5()[3]);

    }

    private String respuestas6[] = {"A. Aun no lo sé ",
        "B. En una veterinaria",
        "C. En casa de algún pariente ",
        "D. En una pensión"

    };

    private List<String> lista6 = new ArrayList<String>();

    {

        getLista6().add(getRespuestas6()[0]);
        getLista6().add(getRespuestas6()[1]);
        getLista6().add(getRespuestas6()[2]);
        getLista6().add(getRespuestas6()[3]);

    }

    private String respuestas7[] = {" A Alérgica ",
        "B. Iracunda, grita y se altera fácilmente",
        "C. Ninguna de las anteriores ",
        "D. Adora a los animales igual que yo"

    };

    private List<String> lista7 = new ArrayList<String>();

    {

        getLista7().add(getRespuestas7()[0]);
        getLista7().add(getRespuestas7()[1]);
        getLista7().add(getRespuestas7()[2]);
        getLista7().add(getRespuestas7()[3]);

    }

    private String respuestas8[] = {"A. Se le golpea con periódico  ",
        "B. Se le castiga encerrándolo",
        "C. Nada se debe hacer pues es parte de la naturaleza cuando son cachorros. Conseguirle juguetes que cubran esa función ",
        "D. Se le corrige en el momento con una llamada de atención firme"

    };

    private List<String> lista8 = new ArrayList<String>();

    {

        getLista8().add(getRespuestas8()[0]);
        getLista8().add(getRespuestas8()[1]);
        getLista8().add(getRespuestas8()[2]);
        getLista8().add(getRespuestas8()[3]);

    }

    private String respuestas9[] = {"A. No tengo hijos ",
        "B. Reservado, apático",
        "C. Amable, agradable y activo ",
        "D.  En ocasiones es  explosivo, no se adapte bien a las situaciones nuevas."

    };

    private List<String> lista9 = new ArrayList<String>();

    {

        getLista9().add(getRespuestas9()[0]);
        getLista9().add(getRespuestas9()[1]);
        getLista9().add(getRespuestas9()[2]);
        getLista9().add(getRespuestas9()[3]);

    }

    /**
     * @return the cont
     */
    ;
    public int getCont() {

//       pregunta1
        if (pregunta1.equals(getRespuestas()[0])) {
            cont = cont - 2;
        }

        if (pregunta1.equals(getRespuestas()[1])) {
            cont = cont - 1;
        }
        if (pregunta1.equals(getRespuestas()[2])) {
            cont = cont + 1;
        }
        if (pregunta1.equals(getRespuestas()[3])) {
            cont = cont + 2;
        }

//           pregunta 2
        if (pregunta2.equals(getRespuestas2()[0])) {
            cont = cont - 2;
        }

        if (pregunta2.equals(getRespuestas2()[1])) {
            cont = cont - 1;
        }
        if (pregunta2.equals(getRespuestas2()[2])) {
            cont = cont + 1;
        }
        if (pregunta2.equals(getRespuestas2()[3])) {
            cont = cont + 2;
        }

        //           pregunta 3
        if (pregunta3.equals(getRespuestas3()[0])) {
            cont = cont - 2;
        }

        if (pregunta3.equals(getRespuestas3()[1])) {
            cont = cont - 1;
        }
        if (pregunta3.equals(getRespuestas3()[2])) {
            cont = cont + 1;
        }
        if (pregunta3.equals(getRespuestas3()[3])) {
            cont = cont + 2;
        }

        //           pregunta 4
        if (pregunta4.equals(getRespuestas4()[0])) {
            cont = cont - 2;
        }

        if (pregunta4.equals(getRespuestas4()[1])) {
            cont = cont - 1;
        }
        if (pregunta4.equals(getRespuestas4()[2])) {
            cont = cont + 1;
        }
        if (pregunta4.equals(getRespuestas4()[3])) {
            cont = cont + 2;
        }

        //           pregunta 5
        if (pregunta5.equals(getRespuestas5()[0])) {
            cont = cont - 2;
        }

        if (pregunta5.equals(getRespuestas5()[1])) {
            cont = cont - 1;
        }
        if (pregunta5.equals(getRespuestas5()[2])) {
            cont = cont + 1;
        }
        if (pregunta5.equals(getRespuestas5()[3])) {
            cont = cont + 2;
        }


        //           pregunta 6
        if (pregunta6.equals(getRespuestas6()[0])) {
            cont = cont - 2;
        }

        if (pregunta6.equals(getRespuestas6()[1])) {
            cont = cont - 1;
        }
        if (pregunta6.equals(getRespuestas6()[2])) {
            cont = cont + 1;
        }
        if (pregunta6.equals(getRespuestas6()[3])) {
            cont = cont + 2;
        }

        //           pregunta 7
        if (pregunta7.equals(getRespuestas7()[0])) {
            cont = cont - 2;
        }

        if (pregunta7.equals(getRespuestas7()[1])) {
            cont = cont - 1;
        }
        if (pregunta7.equals(getRespuestas7()[2])) {
            cont = cont + 1;
        }
        if (pregunta7.equals(getRespuestas7()[3])) {
            cont = cont + 2;
        }

        //           pregunta 8
        if (pregunta8.equals(getRespuestas8()[0])) {
            cont = cont - 2;
        }

        if (pregunta8.equals(getRespuestas8()[1])) {
            cont = cont - 1;
        }
        if (pregunta8.equals(getRespuestas8()[2])) {
            cont = cont + 1;
        }
        if (pregunta8.equals(getRespuestas8()[3])) {
            cont = cont + 2;
        }

        //           pregunta 9
        if (pregunta9.equals(getRespuestas9()[0])) {
            cont = cont + 1;
        }

        if (pregunta9.equals(getRespuestas9()[1])) {
            cont = cont - 1;
        }
        if (pregunta9.equals(getRespuestas9()[2])) {
            cont = cont + 2;
        }
        if (pregunta9.equals(getRespuestas9()[3])) {
            cont = cont - 2;
        }


        return cont;
    }

    /**
     * @param cont the cont to set
     */
    public void setCont(int cont) {
        this.cont = cont;
    }

    /**
     * @return the pregunta1
     */
    public String getPregunta1() {
        return pregunta1;
    }

    /**
     * @param pregunta1 the pregunta1 to set
     */
    public void setPregunta1(String pregunta1) {
        this.pregunta1 = pregunta1;
    }

    /**
     * @return the pregunta2
     */
    public String getPregunta2() {
        return pregunta2;
    }

    /**
     * @param pregunta2 the pregunta2 to set
     */
    public void setPregunta2(String pregunta2) {
        this.pregunta2 = pregunta2;
    }

    /**
     * @return the pregunta3
     */
    public String getPregunta3() {
        return pregunta3;
    }

    /**
     * @param pregunta3 the pregunta3 to set
     */
    public void setPregunta3(String pregunta3) {
        this.pregunta3 = pregunta3;
    }

    /**
     * @return the pregunta4
     */
    public String getPregunta4() {
        return pregunta4;
    }

    /**
     * @param pregunta4 the pregunta4 to set
     */
    public void setPregunta4(String pregunta4) {
        this.pregunta4 = pregunta4;
    }

    /**
     * @return the pregunta5
     */
    public String getPregunta5() {
        return pregunta5;
    }

    /**
     * @param pregunta5 the pregunta5 to set
     */
    public void setPregunta5(String pregunta5) {
        this.pregunta5 = pregunta5;
    }

    /**
     * @return the pregunta6
     */
    public String getPregunta6() {
        return pregunta6;
    }

    /**
     * @param pregunta6 the pregunta6 to set
     */
    public void setPregunta6(String pregunta6) {
        this.pregunta6 = pregunta6;
    }

    /**
     * @return the pregunta7
     */
    public String getPregunta7() {
        return pregunta7;
    }

    /**
     * @param pregunta7 the pregunta7 to set
     */
    public void setPregunta7(String pregunta7) {
        this.pregunta7 = pregunta7;
    }

    /**
     * @return the pregunta8
     */
    public String getPregunta8() {
        return pregunta8;
    }

    /**
     * @param pregunta8 the pregunta8 to set
     */
    public void setPregunta8(String pregunta8) {
        this.pregunta8 = pregunta8;
    }

    /**
     * @return the pregunta9
     */
    public String getPregunta9() {
        return pregunta9;
    }

    /**
     * @param pregunta9 the pregunta9 to set
     */
    public void setPregunta9(String pregunta9) {
        this.pregunta9 = pregunta9;
    }

    /**
     * @return the respuestas
     */
    public String[] getRespuestas() {
        return respuestas;
    }

    /**
     * @param respuestas the respuestas to set
     */
    public void setRespuestas(String[] respuestas) {
        this.setRespuestas(respuestas);
    }

    /**
     * @return the lista
     */
    public List<String> getLista() {
        return lista;
    }

    /**
     * @param lista the lista to set
     */
    public void setLista(List<String> lista) {
        this.lista = lista;
    }

    /**
     * @param respuestas the respuestas to set
     */
    /**
     * @return the respuestas2
     */
    public String[] getRespuestas2() {
        return respuestas2;
    }

    /**
     * @param respuestas2 the respuestas2 to set
     */
    public void setRespuestas2(String[] respuestas2) {
        this.respuestas2 = respuestas2;
    }

    /**
     * @return the lista2
     */
    public List<String> getLista2() {
        return lista2;
    }

    /**
     * @param lista2 the lista2 to set
     */
    public void setLista2(List<String> lista2) {
        this.lista2 = lista2;
    }

    /**
     * @return the respuestas3
     */
    public String[] getRespuestas3() {
        return respuestas3;
    }

    /**
     * @param respuestas3 the respuestas3 to set
     */
    public void setRespuestas3(String[] respuestas3) {
        this.respuestas3 = respuestas3;
    }

    /**
     * @return the lista3
     */
    public List<String> getLista3() {
        return lista3;
    }

    /**
     * @param lista3 the lista3 to set
     */
    public void setLista3(List<String> lista3) {
        this.lista3 = lista3;
    }

    /**
     * @return the respuestas4
     */
    public String[] getRespuestas4() {
        return respuestas4;
    }

    /**
     * @param respuestas4 the respuestas4 to set
     */
    public void setRespuestas4(String[] respuestas4) {
        this.respuestas4 = respuestas4;
    }

    /**
     * @return the lista4
     */
    public List<String> getLista4() {
        return lista4;
    }

    /**
     * @param lista4 the lista4 to set
     */
    public void setLista4(List<String> lista4) {
        this.lista4 = lista4;
    }

    /**
     * @return the respuestas5
     */
    public String[] getRespuestas5() {
        return respuestas5;
    }

    /**
     * @param respuestas5 the respuestas5 to set
     */
    public void setRespuestas5(String[] respuestas5) {
        this.respuestas5 = respuestas5;
    }

    /**
     * @return the lista5
     */
    public List<String> getLista5() {
        return lista5;
    }

    /**
     * @param lista5 the lista5 to set
     */
    public void setLista5(List<String> lista5) {
        this.lista5 = lista5;
    }

    /**
     * @return the respuestas6
     */
    public String[] getRespuestas6() {
        return respuestas6;
    }

    /**
     * @param respuestas6 the respuestas6 to set
     */
    public void setRespuestas6(String[] respuestas6) {
        this.respuestas6 = respuestas6;
    }

    /**
     * @return the lista6
     */
    public List<String> getLista6() {
        return lista6;
    }

    /**
     * @param lista6 the lista6 to set
     */
    public void setLista6(List<String> lista6) {
        this.lista6 = lista6;
    }

    /**
     * @return the respuestas7
     */
    public String[] getRespuestas7() {
        return respuestas7;
    }

    /**
     * @param respuestas7 the respuestas7 to set
     */
    public void setRespuestas7(String[] respuestas7) {
        this.respuestas7 = respuestas7;
    }

    /**
     * @return the lista7
     */
    public List<String> getLista7() {
        return lista7;
    }

    /**
     * @param lista7 the lista7 to set
     */
    public void setLista7(List<String> lista7) {
        this.lista7 = lista7;
    }

    /**
     * @return the respuestas8
     */
    public String[] getRespuestas8() {
        return respuestas8;
    }

    /**
     * @param respuestas8 the respuestas8 to set
     */
    public void setRespuestas8(String[] respuestas8) {
        this.respuestas8 = respuestas8;
    }

    /**
     * @return the lista8
     */
    public List<String> getLista8() {
        return lista8;
    }

    /**
     * @param lista8 the lista8 to set
     */
    public void setLista8(List<String> lista8) {
        this.lista8 = lista8;
    }

    /**
     * @return the respuestas9
     */
    public String[] getRespuestas9() {
        return respuestas9;
    }

    /**
     * @param respuestas9 the respuestas9 to set
     */
    public void setRespuestas9(String[] respuestas9) {
        this.respuestas9 = respuestas9;
    }

    /**
     * @return the lista9
     */
    public List<String> getLista9() {
        return lista9;
    }

    /**
     * @param lista9 the lista9 to set
     */
    public void setLista9(List<String> lista9) {
        this.lista9 = lista9;
    }

}
