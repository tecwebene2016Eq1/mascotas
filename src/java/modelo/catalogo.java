/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Alex
 */
public class catalogo {

    private String nombre;

    private String especie;

    private String raza;

    private int tama;

    private String desc;

    private String foto;

    private char sexo;

    private String senas;

    private Date fecha;

    private String rut;

    Connection cnn;
    Statement state;
    ResultSet res;

    public catalogo() {

        try {
            Class.forName("com.mysql.jdbc.Driver");
            cnn = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/mascota?zeroDateTimeBehavior=convertToNull", "root", "");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(catalogo.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(catalogo.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public catalogo(String nombre, String especie, String raza, int tama, String desc, String foto, char sexo, String senas, Date fecha) {

        this.nombre = nombre;
        this.especie = especie;

        this.raza = raza;
        this.tama = tama;

        this.desc = desc;

        this.foto = foto;

        this.sexo = sexo;

        this.senas = senas;

        this.fecha = fecha;

    }

//    public List<Mascota>  consultar() throws SQLException {
//
//        String query = "select * from mascotas";
//        PreparedStatement preparedStatement = cnn.prepareStatement(query);
//        
//        ResultSet result = preparedStatement.executeQuery();
//        
//        
//    }
    
    

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the especie
     */
    public String getEspecie() {
        return especie;
    }

    /**
     * @param especie the especie to set
     */
    public void setEspecie(String especie) {
        this.especie = especie;
    }

    /**
     * @return the raza
     */
    public String getRaza() {
        return raza;
    }

    /**
     * @param raza the raza to set
     */
    public void setRaza(String raza) {
        this.raza = raza;
    }

    /**
     * @return the tama
     */
    public int getTama() {
        return tama;
    }

    /**
     * @param tama the tama to set
     */
    public void setTama(int tama) {
        this.tama = tama;
    }

    /**
     * @return the desc
     */
    public String getDesc() {
        return desc;
    }

    /**
     * @param desc the desc to set
     */
    public void setDesc(String desc) {
        this.desc = desc;
    }

    /**
     * @return the foto
     */
    public String getFoto() {
        return foto;
    }

    /**
     * @param foto the foto to set
     */
    public void setFoto(String foto) {
        this.foto = foto;
    }

    /**
     * @return the sexo
     */
    public char getSexo() {
        return sexo;
    }

    /**
     * @param sexo the sexo to set
     */
    public void setSexo(char sexo) {
        this.sexo = sexo;
    }

    /**
     * @return the senas
     */
    public String getSenas() {
        return senas;
    }

    /**
     * @param senas the senas to set
     */
    public void setSenas(String senas) {
        this.senas = senas;
    }

    /**
     * @return the fecha
     */
    public Date getFecha() {
        return fecha;
    }

    /**
     * @param fecha the fecha to set
     */
    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    /**
     * @return the rut
     */
    public String getRut() {
        return rut;
    }

    /**
     * @param rut the rut to set
     */
    public void setRut(String rut) {
        this.rut = rut;
    }

}
