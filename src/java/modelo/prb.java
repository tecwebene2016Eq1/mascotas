/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;

/**
 *
 * @author Alex
 */
@Named(value = "prb")
@RequestScoped

public class prb implements Serializable {

    /**
     * Creates a new instance of prb
     */
    private int cont;
    private String pregunta1;
    private String pregunta2;
    
    
    
    private List<String> lista = new ArrayList<String>();

    {
        getLista().add("hola");
        getLista().add("hola2");
        getLista().add("hola3");
        getLista().add("hola4");

    }

    public void conta() {

        if (pregunta1.equals("hola2")) {

            cont = 2;

        }

    }

    /**
     * @return the cont
     */
    public int getCont() {

        if (pregunta1.equals("hola2")) {

            return 3;

        } else {

            return 5;
        }

    }

    /**
     * @param cont the cont to set
     */
    public void setCont(int cont) {

        this.cont = cont;

    }

    /**
     * @return the pregunta1
     */
    public String getPregunta1() {

        return pregunta1;

    }

    /**
     * @param pregunta1 the pregunta1 to set
     */
    public void setPregunta1(String pregunta1) {
        this.pregunta1 = pregunta1;

    }

    /**
     * @return the pregunta2
     */
    public String getPregunta2() {
        return pregunta2;
    }

    /**
     * @param pregunta2 the pregunta2 to set
     */
    public void setPregunta2(String pregunta2) {
        this.pregunta2 = pregunta2;
    }

    /**
     * @return the lista
     */
    public List<String> getLista() {
        return lista;
    }

    /**
     * @param lista the lista to set
     */
    public void setLista(List<String> lista) {
        this.lista = lista;
    }

}
