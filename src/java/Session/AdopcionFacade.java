/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Session;

import Entity.Adopcion;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Djx
 */
@Stateless
public class AdopcionFacade extends AbstractFacade<Adopcion> {

    @PersistenceContext(unitName = "TDW_Mascotas_FinalPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AdopcionFacade() {
        super(Adopcion.class);
    }
    
}
