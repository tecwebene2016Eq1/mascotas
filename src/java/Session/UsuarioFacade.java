/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Session;

import Entity.Usuario;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author chef
 */
@Stateless
public class UsuarioFacade extends AbstractFacade<Usuario> implements UsuarioFacadeLocal {

    @PersistenceContext(unitName = "TDW_Mascotas_FinalPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UsuarioFacade() {
        super(Usuario.class);
    }
    
    @Override
    public Usuario iniciarsession(Usuario us){
        Usuario usr = null;
        String Conuslta; 
        try {
            Conuslta = "From Usuario u where u.usuario= ?1 and u.password = ?2";
            Query query= em.createQuery(Conuslta);
            query.setParameter(1, us.getUsuario());
            query.setParameter(2, us.getPassword());
            List<Usuario> lista =query.getResultList();
            if(!lista.isEmpty()){
                usr = lista.get(0); 
            }
        } catch (Exception e) {
            throw e;
            
        }
        return usr;
    }
    
}
