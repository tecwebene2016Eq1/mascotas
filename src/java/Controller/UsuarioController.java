/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Controller.util.JsfUtil;
import Entity.Adoptante;
import Entity.Usuario;
import Entity.Veterinario;
import Session.AdoptanteFacade;
import Session.UsuarioFacadeLocal;
import Session.VeterinarioFacade;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.inject.Named;

/**
 *
 * @author chef
 */
@Named
@SessionScoped
public class UsuarioController implements Serializable {

    @EJB
    private UsuarioFacadeLocal userEJB;
    
    @EJB
    private VeterinarioFacade vetEJB;
    
    @EJB
    private AdoptanteFacade adoptanteEJB;
    
    private Usuario User;
    private Veterinario vet;
    private Adoptante adopta;

    @PostConstruct
    public void init() {
        User = new Usuario();
        vet = new Veterinario();
        adopta = new Adoptante();
    }

    /**
     * @return the User
     */
    public Usuario getUser() {
        return User;
    }

    /**
     * @param User the User to set
     */
    public void setUser(Usuario User) {
        this.User = User;
    }

    public void registrarveterinario() {
        try {
            User.setRol(1);
            this.vet.setUsuarioIdusuario(User);
            vetEJB.create(vet);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Aviso:", "Usuario Registrado"));
            System.out.println("Se registro");
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Aviso", "Usuario No registrado"));
        }
    }
    public String registraruser() {
        try {
            User.setRol(2);
            this.adopta.setUsuarioIdusuario(User);
            adoptanteEJB.create(adopta);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Aviso:", "Usuario Registrado"));
            System.out.println("Se registro");
            User = new Usuario();
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Aviso", "Usuario No registrado"));
        }return "/index";
    }

    public String iniciarSession() {
        String redireccion = null;
        Usuario us;
        try {
            us = userEJB.iniciarsession(User);
            if (us != null) {
                if (us.getRol() == 1) {
                    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("usuario", us);
                    redireccion = "/expedientes/List?faces-redirect=true";
                }else if (us.getRol() == 0) {
                    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("usuario", us);
                    redireccion = "/Admin/admin?faces-redirect=true";
                }
                else if(us.getRol()==2) {
                    FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("usuario", us);
                    redireccion = "/sisexp/catalogo?faces-redirect=true";
                }

            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Aviso", "Usuario con credenciales Incorrectas"));
            }
        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Aviso", "Error:"));
        }
        return redireccion;
    }

    public void VerificarSession() {
        try {
            Usuario us = (Usuario) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("usuario");
            if (us == null) {
                FacesContext.getCurrentInstance().getExternalContext().redirect("/index.xhtml");
            }
        } catch (Exception e) {
        }
    }

    public void VerificarSessionVeterinario() {
        try {
            Usuario us = (Usuario) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("usuario");
            if (us == null||us.getRol()!=1) {
                FacesContext.getCurrentInstance().getExternalContext().redirect("/index.xhtml");
            } 
        } catch (Exception e) {
        }
    }
    public void VerificarSessionAdmin() {
        try {
            Usuario us = (Usuario) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("usuario");
            if (us == null||us.getRol()!=0) {
                FacesContext.getCurrentInstance().getExternalContext().redirect("./../index.xhtml");
            } 
        } catch (Exception e) {
        }
    }
    public void VerificarSessionUsuario() {
        try {
            Usuario us = (Usuario) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("usuario");
            if (us == null||us.getRol()!=2) {
                FacesContext.getCurrentInstance().getExternalContext().redirect("./../index.xhtml");
            } 
        } catch (Exception e) {
        }
    }

     public String cerrarSession() {
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        return "/index.xhtml";
    }
     
    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(userEJB.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(userEJB.findAll(), true);
    }

    /**
     * @return the vet
     */
    public Veterinario getVet() {
        return vet;
    }

    /**
     * @param vet the vet to set
     */
    public void setVet(Veterinario vet) {
        this.vet = vet;
    }

    /**
     * @return the adopta
     */
    public Adoptante getAdopta() {
        return adopta;
    }

    /**
     * @param adopta the adopta to set
     */
    public void setAdopta(Adoptante adopta) {
        this.adopta = adopta;
    }
}
