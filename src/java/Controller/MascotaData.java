/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import modelo.Mascota;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.faces.model.DataModel;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 *
 * @author Djx
 */
@Named(value = "mascotaData")
@SessionScoped
public class MascotaData implements Serializable {

    @Resource(name = "java:app/mascota")
    private DataSource ds;

    /**
     * Creates a new instance of MascotaData
     *
     * @return
     * @throws java.sql.SQLException
     */
    public List<Mascota> getMascotaList() throws SQLException {

        try {
            Context ctx = new InitialContext();
            ds = (DataSource) ctx.lookup("java:app/mascota");
        } catch (NamingException e) {
            e.printStackTrace();
            
        }

        if (ds == null) {
            throw new SQLException("Can't get data source");
        }

        //get database connection
        Connection con = ds.getConnection();

        if (con == null) {
            con.close();
            throw new SQLException("Can't get database connection");
        }
        
        String selectSQL = "SELECT m.* FROM mascotas m, expediente e WHERE m.idmascotas = e.mascotas_idmascotas and e.edoSalud=1  ";
        PreparedStatement preparedStatement = con.prepareStatement(selectSQL);
        ResultSet result = preparedStatement.executeQuery();
        List<Mascota> list = new ArrayList<Mascota>();
        
        while (result.next()) {
            Mascota masc = new Mascota();
            masc.setIdmascotas(result.getInt("idmascotas"));
            masc.setNombre(result.getString("nombre"));
            masc.setEspecie(result.getString("especie"));
            masc.setRaza(result.getString("raza"));
            masc.setTamanio(result.getInt("tamanio"));
            masc.setDescripcion(result.getString("descripcion"));
                    masc.setFoto(result.getString("foto"));
            masc.setSexo(result.getString("sexo").charAt(0));
            masc.setColor(result.getString("color"));
            masc.setsParticulares(result.getString("sParticulares"));
            masc.setFechaNacimiento(result.getDate("fechaNacimiento"));

            String selectSQL2 = "SELECT * FROM adopcion where  mascotas_idmascotas=" + result.getInt("idmascotas");
            PreparedStatement preparedStatement2 = con.prepareStatement(selectSQL2);
            ResultSet result2 = preparedStatement2.executeQuery();

            if (result2.next()) {
                if (result2.getInt("adopcionAprobada") == 0) {
                    list.add(masc);
                }
            } else {
                list.add(masc);
            }
        }
        con.close();
        return list;
    }

}
