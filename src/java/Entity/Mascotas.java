/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Djx
 */
@Entity
@Table(name = "mascotas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Mascotas.findAll", query = "SELECT m FROM Mascotas m"),
    @NamedQuery(name = "Mascotas.findByIdmascotas", query = "SELECT m FROM Mascotas m WHERE m.idmascotas = :idmascotas"),
    @NamedQuery(name = "Mascotas.findByNombre", query = "SELECT m FROM Mascotas m WHERE m.nombre = :nombre"),
    @NamedQuery(name = "Mascotas.findByEspecie", query = "SELECT m FROM Mascotas m WHERE m.especie = :especie"),
    @NamedQuery(name = "Mascotas.findByRaza", query = "SELECT m FROM Mascotas m WHERE m.raza = :raza"),
    @NamedQuery(name = "Mascotas.findByTamanio", query = "SELECT m FROM Mascotas m WHERE m.tamanio = :tamanio"),
    @NamedQuery(name = "Mascotas.findByDescripcion", query = "SELECT m FROM Mascotas m WHERE m.descripcion = :descripcion"),
    @NamedQuery(name = "Mascotas.findByFoto", query = "SELECT m FROM Mascotas m WHERE m.foto = :foto"),
    @NamedQuery(name = "Mascotas.findBySexo", query = "SELECT m FROM Mascotas m WHERE m.sexo = :sexo"),
    @NamedQuery(name = "Mascotas.findByColor", query = "SELECT m FROM Mascotas m WHERE m.color = :color"),
    @NamedQuery(name = "Mascotas.findBySParticulares", query = "SELECT m FROM Mascotas m WHERE m.sParticulares = :sParticulares"),
    @NamedQuery(name = "Mascotas.findByFechaNacimiento", query = "SELECT m FROM Mascotas m WHERE m.fechaNacimiento = :fechaNacimiento")})
public class Mascotas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idmascotas")
    private Integer idmascotas;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "nombre")
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "especie")
    private String especie;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "raza")
    private String raza;
    @Basic(optional = false)
    @NotNull
    @Column(name = "tamanio")
    private int tamanio;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "descripcion")
    private String descripcion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4)
    @Column(name = "foto")
    private String foto;
    @Basic(optional = false)
    @NotNull
    @Column(name = "sexo")
    private Character sexo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "color")
    private String color;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "sParticulares")
    private String sParticulares;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fechaNacimiento")
    @Temporal(TemporalType.DATE)
    private Date fechaNacimiento;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "mascotasIdmascotas")
    private Collection<Expediente> expedienteCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "mascotasIdmascotas")
    private Collection<Adopcion> adopcionCollection;

    public Mascotas() {
    }

    public Mascotas(Integer idmascotas) {
        this.idmascotas = idmascotas;
    }

    public Mascotas(Integer idmascotas, String nombre, String especie, String raza, int tamanio, String descripcion, String foto, Character sexo, String color, String sParticulares, Date fechaNacimiento) {
        this.idmascotas = idmascotas;
        this.nombre = nombre;
        this.especie = especie;
        this.raza = raza;
        this.tamanio = tamanio;
        this.descripcion = descripcion;
        this.foto = foto;
        this.sexo = sexo;
        this.color = color;
        this.sParticulares = sParticulares;
        this.fechaNacimiento = fechaNacimiento;
    }

    public Integer getIdmascotas() {
        return idmascotas;
    }

    public void setIdmascotas(Integer idmascotas) {
        this.idmascotas = idmascotas;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEspecie() {
        return especie;
    }

    public void setEspecie(String especie) {
        this.especie = especie;
    }

    public String getRaza() {
        return raza;
    }

    public void setRaza(String raza) {
        this.raza = raza;
    }

    public int getTamanio() {
        return tamanio;
    }

    public void setTamanio(int tamanio) {
        this.tamanio = tamanio;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public Character getSexo() {
        return sexo;
    }

    public void setSexo(Character sexo) {
        this.sexo = sexo;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getSParticulares() {
        return sParticulares;
    }

    public void setSParticulares(String sParticulares) {
        this.sParticulares = sParticulares;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    @XmlTransient
    public Collection<Expediente> getExpedienteCollection() {
        return expedienteCollection;
    }

    public void setExpedienteCollection(Collection<Expediente> expedienteCollection) {
        this.expedienteCollection = expedienteCollection;
    }

    @XmlTransient
    public Collection<Adopcion> getAdopcionCollection() {
        return adopcionCollection;
    }

    public void setAdopcionCollection(Collection<Adopcion> adopcionCollection) {
        this.adopcionCollection = adopcionCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idmascotas != null ? idmascotas.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Mascotas)) {
            return false;
        }
        Mascotas other = (Mascotas) object;
        if ((this.idmascotas == null && other.idmascotas != null) || (this.idmascotas != null && !this.idmascotas.equals(other.idmascotas))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entity.Mascotas[ idmascotas=" + idmascotas + " ]";
    }
    
}
