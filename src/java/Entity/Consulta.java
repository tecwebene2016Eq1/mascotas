/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Djx
 */
@Entity
@Table(name = "consulta")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Consulta.findAll", query = "SELECT c FROM Consulta c"),
    @NamedQuery(name = "Consulta.findByIdcitas", query = "SELECT c FROM Consulta c WHERE c.idcitas = :idcitas"),
    @NamedQuery(name = "Consulta.findByTratamiento", query = "SELECT c FROM Consulta c WHERE c.tratamiento = :tratamiento"),
    @NamedQuery(name = "Consulta.findByFecha", query = "SELECT c FROM Consulta c WHERE c.fecha = :fecha"),
    @NamedQuery(name = "Consulta.findByDescrip", query = "SELECT c FROM Consulta c WHERE c.descrip = :descrip")})
public class Consulta implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idcitas")
    private Integer idcitas;
    @Size(max = 45)
    @Column(name = "tratamiento")
    private String tratamiento;
    @Column(name = "fecha")
    @Temporal(TemporalType.DATE)
    private Date fecha;
    @Size(max = 45)
    @Column(name = "descrip")
    private String descrip;
    @JoinColumn(name = "Veterinario_idVeterinario", referencedColumnName = "idVeterinario")
    @ManyToOne(optional = false)
    private Veterinario veterinarioidVeterinario;
    @JoinColumn(name = "expediente_idexpediente", referencedColumnName = "idexpediente")
    @ManyToOne(optional = false)
    private Expediente expedienteIdexpediente;

    public Consulta() {
    }

    public Consulta(Integer idcitas) {
        this.idcitas = idcitas;
    }

    public Integer getIdcitas() {
        return idcitas;
    }

    public void setIdcitas(Integer idcitas) {
        this.idcitas = idcitas;
    }

    public String getTratamiento() {
        return tratamiento;
    }

    public void setTratamiento(String tratamiento) {
        this.tratamiento = tratamiento;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getDescrip() {
        return descrip;
    }

    public void setDescrip(String descrip) {
        this.descrip = descrip;
    }

    public Veterinario getVeterinarioidVeterinario() {
        return veterinarioidVeterinario;
    }

    public void setVeterinarioidVeterinario(Veterinario veterinarioidVeterinario) {
        this.veterinarioidVeterinario = veterinarioidVeterinario;
    }

    public Expediente getExpedienteIdexpediente() {
        return expedienteIdexpediente;
    }

    public void setExpedienteIdexpediente(Expediente expedienteIdexpediente) {
        this.expedienteIdexpediente = expedienteIdexpediente;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idcitas != null ? idcitas.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Consulta)) {
            return false;
        }
        Consulta other = (Consulta) object;
        if ((this.idcitas == null && other.idcitas != null) || (this.idcitas != null && !this.idcitas.equals(other.idcitas))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entity.Consulta[ idcitas=" + idcitas + " ]";
    }
    
}
