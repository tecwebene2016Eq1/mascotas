/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Djx
 */
@Entity
@Table(name = "adopcion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Adopcion.findAll", query = "SELECT a FROM Adopcion a"),
    @NamedQuery(name = "Adopcion.findByIdadopcion", query = "SELECT a FROM Adopcion a WHERE a.idadopcion = :idadopcion"),
    @NamedQuery(name = "Adopcion.findByResultado", query = "SELECT a FROM Adopcion a WHERE a.resultado = :resultado"),
    @NamedQuery(name = "Adopcion.findByAdopcionAprobada", query = "SELECT a FROM Adopcion a WHERE a.adopcionAprobada = :adopcionAprobada")})
public class Adopcion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idadopcion")
    private Integer idadopcion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "resultado")
    private int resultado;
    @Column(name = "adopcionAprobada")
    private Integer adopcionAprobada;
    @JoinColumn(name = "adoptante_idAdoptante", referencedColumnName = "idAdoptante")
    @ManyToOne(optional = false)
    private Adoptante adoptanteidAdoptante;
    @JoinColumn(name = "mascotas_idmascotas", referencedColumnName = "idmascotas")
    @ManyToOne(optional = false)
    private Mascotas mascotasIdmascotas;

    public Adopcion() {
    }

    public Adopcion(Integer idadopcion) {
        this.idadopcion = idadopcion;
    }

    public Adopcion(Integer idadopcion, int resultado) {
        this.idadopcion = idadopcion;
        this.resultado = resultado;
    }

    public Integer getIdadopcion() {
        return idadopcion;
    }

    public void setIdadopcion(Integer idadopcion) {
        this.idadopcion = idadopcion;
    }

    public int getResultado() {
        return resultado;
    }

    public void setResultado(int resultado) {
        this.resultado = resultado;
    }

    public Integer getAdopcionAprobada() {
        return adopcionAprobada;
    }

    public void setAdopcionAprobada(Integer adopcionAprobada) {
        this.adopcionAprobada = adopcionAprobada;
    }

    public Adoptante getAdoptanteidAdoptante() {
        return adoptanteidAdoptante;
    }

    public void setAdoptanteidAdoptante(Adoptante adoptanteidAdoptante) {
        this.adoptanteidAdoptante = adoptanteidAdoptante;
    }

    public Mascotas getMascotasIdmascotas() {
        return mascotasIdmascotas;
    }

    public void setMascotasIdmascotas(Mascotas mascotasIdmascotas) {
        this.mascotasIdmascotas = mascotasIdmascotas;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idadopcion != null ? idadopcion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Adopcion)) {
            return false;
        }
        Adopcion other = (Adopcion) object;
        if ((this.idadopcion == null && other.idadopcion != null) || (this.idadopcion != null && !this.idadopcion.equals(other.idadopcion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entity.Adopcion[ idadopcion=" + idadopcion + " ]";
    }
    
}
