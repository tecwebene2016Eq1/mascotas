/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Djx
 */
@Entity
@Table(name = "expediente")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Expediente.findAll", query = "SELECT e FROM Expediente e"),
    @NamedQuery(name = "Expediente.findByIdexpediente", query = "SELECT e FROM Expediente e WHERE e.idexpediente = :idexpediente"),
    @NamedQuery(name = "Expediente.findByEdoSalud", query = "SELECT e FROM Expediente e WHERE e.edoSalud = :edoSalud")})
public class Expediente implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idexpediente")
    private Integer idexpediente;
    @Column(name = "edoSalud")
    private Integer edoSalud;
    @JoinColumn(name = "mascotas_idmascotas", referencedColumnName = "idmascotas")
    @ManyToOne(optional = false)
    private Mascotas mascotasIdmascotas;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "expedienteIdexpediente")
    private Collection<Consulta> consultaCollection;

    public Expediente() {
    }

    public Expediente(Integer idexpediente) {
        this.idexpediente = idexpediente;
    }

    public Integer getIdexpediente() {
        return idexpediente;
    }

    public void setIdexpediente(Integer idexpediente) {
        this.idexpediente = idexpediente;
    }

    public Integer getEdoSalud() {
        return edoSalud;
    }

    public void setEdoSalud(Integer edoSalud) {
        this.edoSalud = edoSalud;
    }

    public Mascotas getMascotasIdmascotas() {
        return mascotasIdmascotas;
    }

    public void setMascotasIdmascotas(Mascotas mascotasIdmascotas) {
        this.mascotasIdmascotas = mascotasIdmascotas;
    }

    @XmlTransient
    public Collection<Consulta> getConsultaCollection() {
        return consultaCollection;
    }

    public void setConsultaCollection(Collection<Consulta> consultaCollection) {
        this.consultaCollection = consultaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idexpediente != null ? idexpediente.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Expediente)) {
            return false;
        }
        Expediente other = (Expediente) object;
        if ((this.idexpediente == null && other.idexpediente != null) || (this.idexpediente != null && !this.idexpediente.equals(other.idexpediente))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entity.Expediente[ idexpediente=" + idexpediente + " ]";
    }
    
}
