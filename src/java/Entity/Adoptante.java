/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Djx
 */
@Entity
@Table(name = "adoptante")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Adoptante.findAll", query = "SELECT a FROM Adoptante a"),
    @NamedQuery(name = "Adoptante.findByIdAdoptante", query = "SELECT a FROM Adoptante a WHERE a.idAdoptante = :idAdoptante"),
    @NamedQuery(name = "Adoptante.findByOcupacion", query = "SELECT a FROM Adoptante a WHERE a.ocupacion = :ocupacion"),
    @NamedQuery(name = "Adoptante.findByEdad", query = "SELECT a FROM Adoptante a WHERE a.edad = :edad"),
    @NamedQuery(name = "Adoptante.findByIfe", query = "SELECT a FROM Adoptante a WHERE a.ife = :ife"),
    @NamedQuery(name = "Adoptante.findByTipoVivienda", query = "SELECT a FROM Adoptante a WHERE a.tipoVivienda = :tipoVivienda"),
    @NamedQuery(name = "Adoptante.findByTipoPropiedad", query = "SELECT a FROM Adoptante a WHERE a.tipoPropiedad = :tipoPropiedad")})
public class Adoptante implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idAdoptante")
    private Integer idAdoptante;
    @Size(max = 45)
    @Column(name = "ocupacion")
    private String ocupacion;
    @Column(name = "edad")
    private Integer edad;
    @Size(max = 12)
    @Column(name = "ife")
    private String ife;
    @Column(name = "tipoVivienda")
    private Integer tipoVivienda;
    @Column(name = "tipoPropiedad")
    private Integer tipoPropiedad;
    @JoinColumn(name = "usuario_idusuario", referencedColumnName = "idusuario")
    @ManyToOne( cascade = CascadeType.PERSIST ,optional = false)
    private Usuario usuarioIdusuario;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "adoptanteidAdoptante")
    private Collection<Adopcion> adopcionCollection;

    public Adoptante() {
    }

    public Adoptante(Integer idAdoptante) {
        this.idAdoptante = idAdoptante;
    }

    public Integer getIdAdoptante() {
        return idAdoptante;
    }

    public void setIdAdoptante(Integer idAdoptante) {
        this.idAdoptante = idAdoptante;
    }

    public String getOcupacion() {
        return ocupacion;
    }

    public void setOcupacion(String ocupacion) {
        this.ocupacion = ocupacion;
    }

    public Integer getEdad() {
        return edad;
    }

    public void setEdad(Integer edad) {
        this.edad = edad;
    }

    public String getIfe() {
        return ife;
    }

    public void setIfe(String ife) {
        this.ife = ife;
    }

    public Integer getTipoVivienda() {
        return tipoVivienda;
    }

    public void setTipoVivienda(Integer tipoVivienda) {
        this.tipoVivienda = tipoVivienda;
    }

    public Integer getTipoPropiedad() {
        return tipoPropiedad;
    }

    public void setTipoPropiedad(Integer tipoPropiedad) {
        this.tipoPropiedad = tipoPropiedad;
    }

    public Usuario getUsuarioIdusuario() {
        return usuarioIdusuario;
    }

    public void setUsuarioIdusuario(Usuario usuarioIdusuario) {
        this.usuarioIdusuario = usuarioIdusuario;
    }

    @XmlTransient
    public Collection<Adopcion> getAdopcionCollection() {
        return adopcionCollection;
    }

    public void setAdopcionCollection(Collection<Adopcion> adopcionCollection) {
        this.adopcionCollection = adopcionCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAdoptante != null ? idAdoptante.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Adoptante)) {
            return false;
        }
        Adoptante other = (Adoptante) object;
        if ((this.idAdoptante == null && other.idAdoptante != null) || (this.idAdoptante != null && !this.idAdoptante.equals(other.idAdoptante))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entity.Adoptante[ idAdoptante=" + idAdoptante + " ]";
    }
    
}
